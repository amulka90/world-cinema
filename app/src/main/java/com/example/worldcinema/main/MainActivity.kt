package com.example.worldcinema.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.worldcinema.R
import com.example.worldcinema.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.BottomNavigation.setOnNavigationItemSelectedListener {  item ->
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment, Fragment())
                .commit()

            true
        }
    }
}