package com.example.worldcinema.signup

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.worldcinema.databinding.ActivitySignUpBinding
import com.example.worldcinema.signin.SignInActivity

class SignUpActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySignUpBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.alreadyHaveButton.setOnClickListener {
            startActivity(
                Intent(this, SignInActivity::class.java )
            )
            finish()
        }

        binding.registrationButton.setOnClickListener {
            startActivity(
                Intent(this, SignInActivity::class.java)
            )
            finish()
        }
    }
}