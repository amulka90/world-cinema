package com.example.worldcinema.launch

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.example.worldcinema.signup.SignUpActivity
import com.example.worldcinema.databinding.ActivityLaunchScreenBinding

class LaunchScreenActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLaunchScreenBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLaunchScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)


        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(
                Intent(this, SignUpActivity::class.java)
            )
            finish()
        }, 2500
        )
    }
}